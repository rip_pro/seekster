import axios from "axios";
const apiUrl = "https://jsonplaceholder.typicode.com";
const state = {
  users: [],
};
const getters = {
  allUsers: (state) => state.users,
};
const mutations = {
  SET_USERS(state, users) {
    state.users = users;
  },
};

const actions = {
  async getUsers({ commit }) {
    const response = await axios.get(`${apiUrl}/users`);
    commit("SET_USERS", response.data);
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
