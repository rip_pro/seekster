import axios from "axios";
const apiUrl = "https://jsonplaceholder.typicode.com";
const state = {
  guides: [],
};
const getters = {
  allGuides: (state) => state.guides,
};
const actions = {
  async getGuides({ commit }) {
    const response = await axios.get(`${apiUrl}/posts`);
    commit("SET_GUIDES", response.data);
  },
  async addGuides({ commit }, playload) {
    const response = await axios.post(`${apiUrl}/posts`, playload);
    commit("ADD_GUIDES", response.data);
  },
  async deleteGuides({ commit }, id) {
    await axios.delete(`${apiUrl}/posts/${id}`);
    commit("DELETE_GUIDES", id);
  },
  async updateGuides({ commit }, updGuides) {
    console.log("updateGuides", updGuides);
    const response = await axios.put(
      `https://jsonplaceholder.typicode.com/posts/${updGuides.id}`,
      updGuides
    );
    commit("UPDATE_GUIDES", response.data);
  },
};
const mutations = {
  SET_GUIDES(state, guides) {
    state.guides = guides;
  },
  ADD_GUIDES: (state, guide) => state.guides.unshift(guide),
  DELETE_GUIDES: (state, id) =>
    (state.guides = state.guides.filter((guides) => guides.id !== id)),
  UPDATE_GUIDES: (state, updGuide) => {
    const index = state.guides.findIndex((guide) => guide.id === updGuide.id);
    if (index !== -1) {
      state.guides.splice(index, 1, updGuide);
    }
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
