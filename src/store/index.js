import Vuex from "vuex";

import users from "./users";
import guides from "./guides";

// Load Vuex

// Create store
export default new Vuex.Store({
  modules: {
    users,
    guides,
  },
});
